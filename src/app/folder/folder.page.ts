import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MagicBallService } from "../services/magic-ball.service";


@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})


export class FolderPage implements OnInit {
  public folder: string;
  public answer: string = "...";

  constructor(private activatedRoute: ActivatedRoute, public magicBall: MagicBallService) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

  showAnswer() {
    this.answer = this.magicBall.getRandomAnswer();
  }


}
